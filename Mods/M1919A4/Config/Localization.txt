Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish

gunMGT3BrowningM1919,items,Item,,,"Пулемет .30 Калибра"
gunMGT3BrowningM1919Desc,items,Gun,,,"The M1919 Browning is a .30 caliber medium machine gun that was widely used during the 20th century, especially during World War II, the Korean War, and the Vietnam War. The M1919 saw service as a light infantry, coaxial, mounted, aircraft, and anti-aircraft machine gun by the U.S. and many other countries.",,
gunMGT3BrowningM1919Schematic,items,Gun,,,Browning M1919A4 Schematic,,
ammo30mmBulletBall,items,Item,,,"Патрон 30мм"
ammo30mmBulletBallDesc,items,Ammo,,,".30mm ammo is used as ammunition for the Browning M1919A4. Can be crafted at a workbench.",,
ammo30mmBulletHP,items,Item,,,"Патрон 30мм Усиленный"
ammo30mmBulletHPDesc,items,Ammo,,,"Hollow point .30mm ammunition is typically a better choice than the standard option, but it is rendered useless against armored enemies. Used as ammunition for the Browning M1919A4. Can be crafted at a workbench.",,
ammo30mmBulletAP,items,Item,,,"Патрон 30мм Бронебойный"
ammo30mmBulletAPDesc,items,Ammo,,,"Armor Piercing .30mm ammunition is the best ammo for maximum damage against the enemy, but also puts a strain on the weapon itself, having to repair it more often. Ignores 50% of target armor. Used as ammunition for the Browning M1919A4. Can be crafted at a workbench.",,

gunM1919A4Schematic,items,Gun,,,Browning M1919A4 Schematic,,

