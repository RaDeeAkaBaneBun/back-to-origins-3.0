<M1919A4>

	<append xpath="/items" >
	
<item name="gunMGT3BrowningM1919">
	<property name="Tags" value="weapon,ranged,holdBreathAiming,reloadPenalty,gun,barrelAttachments,sideAttachments,smallTopAttachments,magazine,drumMagazine,bottomAttachments,mediumTopAttachments,attFortitude,perkMachineGunner,perkBookAutoWeapons,attachmentsIncluded,canHaveCosmetic"/>
	<property name="CustomIcon" value="M1919A4"/>
	<property name="DisplayType" value="rangedGun"/>
	<property name="HoldType" value="60"/>
	<property name="Meshfile" value="#@modfolder:Resources/gunM1919.unity3d?M1919_13.prefab"/>
	<property name="Material" value="MMachineGunParts"/> <property name="Weight" value="6"/>

	<property name="CrosshairOnAim" value="true"/> 
	<property name="CrosshairUpAfterShot" value="true"/>

	<property name="RepairTools" value="RepairKitAutomaticRiffle"/>
	<property name="DegradationBreaksAfter" value="false"/>

	<property name="SoundJammed" value="weapon_jam"/>
	<property name="Sound_Sight_In" value="rifle_sight_in"/>
	<property name="Sound_Sight_Out" value="rifle_sight_out"/>

	<property name="RepairExpMultiplier" value="10.8"/>
	<property name="EconomicValue" value="1500"/>
	<property name="UnlockedBy" value="gunM1919A4Schematic"/>
	<property name="ShowQuality" value="true"/>
	<property name="Group" value="Ammo/Weapons,Ranged Weapons"/>

	<property class="Action0">
		<property name="Class" value="Ranged"/>
		<property name="Delay" value=".150"/> 
		<property name="Magazine_items" value="ammo30mmBulletBall,ammo30mmBulletHP,ammo30mmBulletAP"/>
		<property name="Sound_start" value="m60_fire"/>
		<property name="Sound_loop" value="m60_fire"/>
		<property name="Sound_empty" value="dryfire"/>
		<property name="Sound_reload" value="m60_reload"/>
		<property name="Sound_end" value="m60_fire_end"/>
		<property name="AutoReload" value="false"/>
		<property name="Particles_muzzle_fire" value="gunfire_m60"/>
		<property name="Particles_muzzle_smoke" value="gunfire_smoke"/>
		<property name="Particles_muzzle_fire_fpv" value="gunfire_m60_fpv"/>
		<property name="Particles_muzzle_smoke_fpv" value="gunfire_smoke_fpv"/>
		<requirement name="CVarCompare" cvar="_underwater" operation="LT" value=".98"/>
	</property>

	<property class="Action1">
		<property name="Class" value="Zoom"/>
		<property name="Zoom_max_out" value="55"/>
		<property name="Zoom_max_in" value="55"/>
		<property name="ScopeCameraOffset" value="-.00062,0,.055"/>
	</property>

	<effect_group name="gunMGT3BrowningM1919">
		<passive_effect name="MaxRange" operation="base_set" value="90" tags="perkMachineGunner"/>
		<passive_effect name="DamageFalloffRange" operation="base_set" value="50" tags="perkMachineGunner"/>
		<passive_effect name="DamageFalloffRange" operation="perc_add" value="-.2,.2" tags="perkMachineGunner"/> <!-- random effective rng -->
		<passive_effect name="EntityDamage" operation="base_add" value="-3" tags="perkMachineGunner"/> <!-- damage offset -->
		<passive_effect name="BlockDamage" operation="base_add" value="-2" tags="perkMachineGunner"/> <!-- damage offset -->
		<passive_effect name="DamageModifier" operation="perc_add" value="2" tags="head"/>
		<passive_effect name="EntityDamage" operation="perc_add" value=".2,1.2" tier="2,6"/>

		<passive_effect name="EntityDamage" operation="perc_add" value=".1,.5" tier="2,6" tags="perkMachineGunner"/> <!-- tier bonus -->
		<passive_effect name="BlockDamage" operation="perc_add" value="-.15,.15" tags="perkMachineGunner"/> <!-- random BlockDmg -->
		<passive_effect name="BlockDamage" operation="perc_add" value=".1,.5" tier="2,6" tags="perkMachineGunner"/> <!-- tier bonus -->

		<passive_effect name="DegradationMax" operation="perc_add" value="-.2,.2" tags="perkMachineGunner"/> <!-- random DegMax -->
		<passive_effect name="RoundsPerMinute" operation="perc_add" value="-.05,.05" tags="perkMachineGunner"/> <!-- random APM -->
		<passive_effect name="MagazineSize" operation="perc_add" value="-.122,.122"/> <!-- random MagazineSize -->
		<passive_effect name="WeaponHandling" operation="perc_add" value="-.08,.08" tags="perkMachineGunner"/> <!-- random WeaponHandling -->

		<passive_effect name="RoundsPerMinute" operation="base_set" value="370" tags="perkMachineGunner"/>
		<passive_effect name="BurstRoundCount" operation="base_set" value="1000" tags="perkMachineGunner"/>

		<passive_effect name="MagazineSize" operation="base_set" value="150" tags="perkMachineGunner"/>
		<passive_effect name="ReloadSpeedMultiplier" operation="base_set" value="1" tags="perkMachineGunner"/>

		<passive_effect name="SpreadDegreesVertical" operation="base_set" value="2.8" tags="perkMachineGunner"/>
		<passive_effect name="SpreadDegreesHorizontal" operation="base_set" value="2.8" tags="perkMachineGunner"/>
		<passive_effect name="SpreadMultiplierAiming" operation="base_set" value=".33" tags="perkMachineGunner"/>
		<passive_effect name="SpreadMultiplierCrouching" operation="base_set" value=".8" tags="perkMachineGunner"/>
		<passive_effect name="SpreadMultiplierWalking" operation="base_set" value="1.5" tags="perkMachineGunner"/>
		<passive_effect name="SpreadMultiplierRunning" operation="base_set" value="2.2" tags="perkMachineGunner"/>

		<passive_effect name="KickDegreesVerticalMin" operation="base_set" value="-.4" tags="perkMachineGunner"/>
		<passive_effect name="KickDegreesVerticalMax" operation="base_set" value="1.1" tags="perkMachineGunner"/>
		<passive_effect name="KickDegreesHorizontalMin" operation="base_set" value="-1" tags="perkMachineGunner"/>
		<passive_effect name="KickDegreesHorizontalMax" operation="base_set" value="1" tags="perkMachineGunner"/>

		<passive_effect name="IncrementalSpreadMultiplier" operation="base_set" value="1.5" tags="perkMachineGunner"/>
		<passive_effect name="WeaponHandling" operation="base_set" value=".83" tags="perkMachineGunner"/>

		<passive_effect name="DegradationMax" operation="base_set" value="600,1950" tier="1,6" tags="perkMachineGunner,perkBookAutoWeapons"/>
		<passive_effect name="DegradationPerUse" operation="base_set" value="1" tags="perkMachineGunner,perkBookAutoWeapons"/>

		<passive_effect name="ModSlots" operation="base_set" value="1,1,2,2,3,4" tier="1,2,3,4,5,6"/>
		<passive_effect name="ModPowerBonus" operation="perc_add" value=".10" tags="EntityDamage,BlockDamage"/>
		<passive_effect name="ModPowerBonus" operation="base_add" value="300" tags="EconomicValue"/>
		<passive_effect name="DismemberChance" operation="base_set" value="3"/>
		<passive_effect name="DismemberChance" operation="base_set" value="9" tags="head"/>				
	</effect_group>
</item>	

<item name="ammo30mmBulletBall">
	<property name="Extends" value="ammo762mmBulletBall"/>
	<property name="Stacknumber" value="980"/> 
	<property name="EconomicValue" value="11"/>
	<property name="CustomIcon" value="ammo30mmBulletBall"/>
	<effect_group name="ammo30mmBulletBall" tiered="false">
		<passive_effect name="EntityDamage" operation="base_set" value="68" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="BlockDamage" operation="base_set" value="8" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="-.8" tags="earth"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="2" tags="wood"/>
	</effect_group>
</item>
		
<item name="ammo30mmBulletHP">
	<property name="Extends" value="ammo30mmBulletBall"/>
	<property name="Stacknumber" value="980"/> 
	<property name="EconomicValue" value="11"/>
	<property name="CustomIcon" value="ammo30mmBulletHP"/>
    <property name="UnlockedBy" value="perkSniperHPAmmo"/>
	<effect_group name="ammo30mmBulletHP" tiered="false">
		<passive_effect name="EntityDamage" operation="base_set" value="76" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="BlockDamage" operation="base_set" value="10" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="-.8" tags="earth"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="2.5" tags="wood"/>
	</effect_group>	
</item>	
	
<item name="ammo30mmBulletAP">
	<property name="Extends" value="ammo30mmBulletBall"/>
	<property name="DisplayType" value="ammoBulletAP"/>
	<property name="Stacknumber" value="980"/> 
	<property name="EconomicValue" value="11"/>
	<property name="CustomIcon" value="ammo30mmBulletAP"/>
	<property name="UnlockedBy" value="perkSniperAPAmmo"/>
	<effect_group name="ammo30mmBulletAP" tiered="false">
        <passive_effect name="BlockDamage" operation="base_set" value="12" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="TargetArmor" operation="perc_add" value="-.5" tags="perkDeadEye,perkMachineGunner"/><display_value name="dTargetArmor" value="-.5"/>
		<passive_effect name="EntityPenetrationCount" operation="base_set" value="1"/><display_value name="dTargetPenetration" value="1"/>	
		<passive_effect name="EntityDamage" operation="base_set" value="86" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="BlockDamage" operation="base_set" value="15" tags="perkDeadEye,perkMachineGunner"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="-.8" tags="earth"/>
		<passive_effect name="DamageModifier" operation="perc_add" value="3" tags="wood"/>
	</effect_group>	
</item>	

<item name="gunM1919A4Schematic">
	<property name="Extends" value="schematicNoQualityMaster"/>
	<property name="CreativeMode" value="Player"/>
	<property name="CustomIcon" value="M1919A4"/>
	<property name="Unlocks" value="gunMGT3BrowningM1919"/>
	<effect_group tiered="false">
		<triggered_effect trigger="onSelfPrimaryActionEnd" action="ModifyCVar" cvar="gunMGT3BrowningM1919" operation="set" value="1"/>
		<triggered_effect trigger="onSelfPrimaryActionEnd" action="GiveExp" exp="150"/>
	</effect_group>
</item>	
    </append>

</M1919A4>