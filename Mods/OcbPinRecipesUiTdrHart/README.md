# Alternative UI for OCB Pin Recipes Mod - 7 Days to Die (A20) Addon

This needs [OCB Pin Recipes Mod][1] installed in order to work. It will
change the UI for better readability. Based on work done by [tdrhart][2]
as posted in the [original mod thread][2] (thanks). I slightly adjusted
it to fit a bit closer to the original UI.

![In-Game Details Pinned](Screens/in-game-detail-pins.png)

## Changelog

### Version 0.1.0

- Initial version

[1]: https://github.com/OCB7D2D/OcbPinRecipes
[2]: https://community.7daystodie.com/topic/26859-pin-recipes-mod/?do=findComment&comment=466832